"""
https://www.codewars.com/kata/52597aa56021e91c93000cb0
Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.

move_zeros([False,1,0,1,2,0,1,3,"a"]) # returns[False,1,1,2,1,3,"a",0,0]
"""


def move_zeros(array):
    lst = []
    zero = []
    for i in array:
        if i != 0 or type(i) == bool:
            lst.append(i)
        else:
            zero.append(i)
    return lst + zero


print(move_zeros(["a", 0, 0, "b", None, "c", "d", 0, 1, False, 0, 1, 0, 3, [], 0, 1, 9, 0, 0, {}, 0, 0, 9]))

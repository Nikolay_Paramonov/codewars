"""
https://www.codewars.com/kata/52685f7382004e774f0001f7

Write a function, which takes a non-negative integer (seconds) as input and returns the time in a human-readable format (HH:MM:SS)

HH = hours, padded to 2 digits, range: 00 - 99
MM = minutes, padded to 2 digits, range: 00 - 59
SS = seconds, padded to 2 digits, range: 00 - 59
The maximum time never exceeds 359999 (99:59:59)

You can find some examples in the test fixtures.
"""


def make_readable(seconds):
    # SS = seconds % 60
    # MM = seconds // 60 % 60
    # HH = seconds // 3600
    #
    # if SS < 10:
    #     SS = f'0{SS}'
    # if MM < 10:
    #     MM = f'0{MM}'
    # if HH < 10:
    #     HH = f'0{HH}'

    return '{:02}:{:02}:{:02}'.format(seconds // 3600, seconds // 60 % 60, seconds % 60)


print(make_readable(86399))

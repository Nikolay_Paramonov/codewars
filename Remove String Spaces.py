"""
https://www.codewars.com/kata/57eae20f5500ad98e50002c5
Simple, remove the spaces from the string, then return the resultant string.
"""


def no_space(x):
    return ''.join(x.split())


print(no_space('8 8 Bi fk8h B 8 BB8B B B  B888 c hl8 BhB fd'))

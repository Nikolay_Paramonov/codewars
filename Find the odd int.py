"""
https://www.codewars.com/kata/54da5a58ea159efa38000836/train/
Given an array, find the integer that appears an odd number of times.
There will always be only one integer that appears an odd number of times.
"""


def find_it(seq):
    return [i for i in seq if seq.count(i) % 2 == 1][0]


print(find_it([1, 1, 2, -2, 5, 2, 4, 4, -1, -2, 5]))
